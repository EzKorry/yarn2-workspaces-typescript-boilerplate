# test

## 실행법

본 프로젝트는 `yarn` 혹은 `yarn install` 할 필요가 없음. (zero-install 적용됨.)

```bash
yarn server build && yarn common build
yarn server start
```

## 제대로 동작하는지 테스트

```bash
curl localhost:4000/test
```

서버에서 아래 결과 나옴

```bash
hello
```
